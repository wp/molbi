package mk.ukim.finki.molbi.repository;

import mk.ukim.finki.molbi.model.base.StudyProgramSubjectProfessor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudyProgramSubjectProfessorRepository extends JpaRepository<StudyProgramSubjectProfessor, String> {
    List<StudyProgramSubjectProfessor> findByStudyProgramSubjectSubjectId(String subjectId);
}
