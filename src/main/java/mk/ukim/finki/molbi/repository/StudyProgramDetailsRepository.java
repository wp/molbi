package mk.ukim.finki.molbi.repository;

import mk.ukim.finki.molbi.model.base.StudyProgramDetails;
import mk.ukim.finki.molbi.model.enums.StudyCycle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudyProgramDetailsRepository extends JpaRepository<StudyProgramDetails, String> {
    List<StudyProgramDetails> findByStudyCycleAndAccreditationYearGreaterThanEqualOrderByAccreditationYearDesc(StudyCycle cycle, String year);
}
