package mk.ukim.finki.molbi.repository;

import mk.ukim.finki.molbi.model.base.Subject;
import mk.ukim.finki.molbi.model.enums.SemesterType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, String> {
}
