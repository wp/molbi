package mk.ukim.finki.molbi.repository;

import mk.ukim.finki.molbi.model.requests.InstallmentPaymentStudentRequest;
import mk.ukim.finki.molbi.model.requests.RequestSession;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InstallmentPaymentStudentRequestRepository extends JpaSpecificationRepository<InstallmentPaymentStudentRequest, Long> {
    Long countByRequestSessionId(Long sessionId);

    List<InstallmentPaymentStudentRequest> findByRequestSession(RequestSession requestSession);

    List<InstallmentPaymentStudentRequest> findByStudentIndex(String index);
}
