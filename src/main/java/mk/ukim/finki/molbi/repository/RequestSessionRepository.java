package mk.ukim.finki.molbi.repository;

import mk.ukim.finki.molbi.model.requests.RequestSession;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestSessionRepository extends JpaSpecificationRepository<RequestSession, Long> {
}
