package mk.ukim.finki.molbi.service.inter;

import jakarta.transaction.Transactional;
import mk.ukim.finki.molbi.model.dtos.StudentRequestDto;
import mk.ukim.finki.molbi.model.requests.InstallmentPaymentStudentRequest;
import mk.ukim.finki.molbi.model.requests.RequestSession;
import org.springframework.data.domain.Page;

import java.util.List;

public interface InstallmentPaymentStudentRequestService {

    @Transactional
    Page<InstallmentPaymentStudentRequest> filterAndPaginateRequests
            (Long sessionId, Integer pageNum, Integer results, Boolean isApproved, Boolean isProcessed,
             Integer installmentsNum, String student);

    @Transactional
    List<InstallmentPaymentStudentRequest> getAllRequestByStudent(String index);

    InstallmentPaymentStudentRequest findById(Long id);

    void create(StudentRequestDto dto);

    void edit(Long id, StudentRequestDto dto);

    void delete(Long id);

    void updateStatus(Long id, Boolean action, String rejectReason);

    void markAsProcessed(Long id);

    Long totalRequestBySession(Long sessionId);

    List<InstallmentPaymentStudentRequest> listAllForSession(RequestSession session);

    void sendMailsToFinanceViceDean();

    void sendMailToStudentAdministrationAfterAAViceDeanApproval(InstallmentPaymentStudentRequest request);

    void sendMailToStudentOnStatusChange(InstallmentPaymentStudentRequest request);
}
