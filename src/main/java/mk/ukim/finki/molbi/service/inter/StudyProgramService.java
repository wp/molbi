package mk.ukim.finki.molbi.service.inter;

import mk.ukim.finki.molbi.model.base.StudyProgram;
import mk.ukim.finki.molbi.model.base.StudyProgramDetails;

import java.util.List;
import java.util.Map;

public interface StudyProgramService {
    StudyProgram findById(String id);

    List<StudyProgram> listAll();

    List<StudyProgramDetails> getStudentAvailableStudyPrograms(String index);

    Map<String, String> getMappedStudentPrograms(String index);

}
