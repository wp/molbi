package mk.ukim.finki.molbi.service.impl;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import mk.ukim.finki.molbi.model.base.Student;
import mk.ukim.finki.molbi.model.base.User;
import mk.ukim.finki.molbi.model.dtos.StudentRequestDto;
import mk.ukim.finki.molbi.model.enums.RequestType;
import mk.ukim.finki.molbi.model.enums.UserRole;
import mk.ukim.finki.molbi.model.exceptions.*;
import mk.ukim.finki.molbi.model.requests.InstallmentPaymentStudentRequest;
import mk.ukim.finki.molbi.model.requests.RequestSession;
import mk.ukim.finki.molbi.repository.EmailRepository;
import mk.ukim.finki.molbi.repository.InstallmentPaymentStudentRequestRepository;
import mk.ukim.finki.molbi.repository.StudentRepository;
import mk.ukim.finki.molbi.repository.UserRepository;
import mk.ukim.finki.molbi.service.inter.InstallmentPaymentStudentRequestService;
import mk.ukim.finki.molbi.service.inter.RequestSessionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static mk.ukim.finki.molbi.service.specification.FieldFilterSpecification.filterEquals;

@Service
@RequiredArgsConstructor
public class InstallmentPaymentStudentRequestServiceImpl implements InstallmentPaymentStudentRequestService {

    private final InstallmentPaymentStudentRequestRepository requestRepository;
    private final StudentRepository studentRepository;
    private final UserRepository userRepository;
    private final EmailRepository emailRepository;
    private final RequestSessionService requestSessionService;

    @Override
    @Transactional
    public Page<InstallmentPaymentStudentRequest> filterAndPaginateRequests(Long sessionId,
                                                                            Integer pageNum,
                                                                            Integer results,
                                                                            Boolean isApproved,
                                                                            Boolean isProcessed,
                                                                            Integer installmentsNum,
                                                                            String student) {

        return findPage(sessionId, pageNum, results, isApproved, isProcessed, installmentsNum, student);
    }

    @Override
    public List<InstallmentPaymentStudentRequest> getAllRequestByStudent(String index) {
        return requestRepository.findByStudentIndex(index);
    }

    @Override
    public InstallmentPaymentStudentRequest findById(Long id) {
        return requestRepository.findById(id).orElseThrow(() -> new InstallmentPaymentStudentRequestNotFoundException(id));
    }

    @Override
    public void create(StudentRequestDto dto) {
        if (dto.getInstallmentsNum() == null || dto.getDescription().isEmpty()) {
            throw new AllFieldsNotFilledException();
        }

        RequestSession session = requestSessionService.getActiveSessionByType(RequestType.INSTALLMENT_PAYMENT);
        Student student = studentRepository.findById(dto.getStudent()).orElseThrow(() -> new StudentNotFoundException(dto.getStudent()));

        InstallmentPaymentStudentRequest request = new InstallmentPaymentStudentRequest();
        request.setInstallmentsNum(dto.getInstallmentsNum());
        request.setDescription(dto.getDescription());
        request.setStudent(student);
        request.setDateCreated(LocalDate.now());
        request.setRequestSession(session);
        request.setIsProcessed(false);
        requestRepository.save(request);
    }

    @Override
    public void edit(Long id, StudentRequestDto dto) {
        InstallmentPaymentStudentRequest request = findById(id);

        if (request.getIsApproved() != null) {
            throw new IllegalStateException("Cannot edit a reviewed request");
        }

        if (dto.getInstallmentsNum() == null || dto.getDescription().isEmpty()) {
            throw new AllFieldsNotFilledException();
        }

        request.setInstallmentsNum(dto.getInstallmentsNum());
        request.setDescription(dto.getDescription());
        request.setDateCreated(LocalDate.now());
        requestRepository.save(request);
    }

    @Override
    public void delete(Long id) {
        InstallmentPaymentStudentRequest request = findById(id);

        if (request.getIsApproved() != null) {
            throw new IllegalStateException("Cannot delete a reviewed request");
        }
        requestRepository.deleteById(id);
    }

    @Override
    public void updateStatus(Long id, Boolean action, String rejectReason) {
        InstallmentPaymentStudentRequest request = findById(id);
        if (action && !request.canBeApproved()) {
            throw new RequestAlreadyProcessedOrAlreadyApprovedException(request);
        }
        if (!action && !request.canBeRejected()) {
            throw new RequestAlreadyProcessedOrAlreadyRejectedException(request);
        }

        request.setIsApproved(action);
        request.setRejectionReason(rejectReason);
        requestRepository.save(request);
        if (request.getIsApproved() != null && request.getIsApproved()) {
            sendMailToStudentAdministrationAfterAAViceDeanApproval(request);
        }
        sendMailToStudentOnStatusChange(request);
    }

    @Override
    public void markAsProcessed(Long id) {
        InstallmentPaymentStudentRequest request = findById(id);
        if (request.getIsApproved() == null || !request.canBeMarkedAsProcessed()) {
            throw new RequestNotApprovedOrAlreadyProcessedException(request);
        }
        request.setIsProcessed(true);
        request.setDateProcessed(LocalDate.now());
        requestRepository.save(request);
        sendMailToStudentOnStatusChange(request);
    }

    @Override
    public Long totalRequestBySession(Long sessionId) {
        return requestRepository.countByRequestSessionId(sessionId);
    }

    @Override
    public List<InstallmentPaymentStudentRequest> listAllForSession(RequestSession session) {
        return requestRepository.findByRequestSession(session);
    }

    @Override
    public void sendMailsToFinanceViceDean() {
        List<RequestSession> endedSessions = requestSessionService.getJustEndedSessions();
        List<InstallmentPaymentStudentRequest> ipStudentRequests = new ArrayList<>();
        for (RequestSession endedSession : endedSessions) {
            ipStudentRequests.addAll(listAllForSession(endedSession));
        }

        User aaViceDean = userRepository.findByRole(UserRole.FINANCES_VICE_DEAN).stream().findFirst().orElseThrow();
        String[] to = {aaViceDean.getEmail()};

        for (InstallmentPaymentStudentRequest ipStudentRequest : ipStudentRequests) {
            Map<String, Object> model = new HashMap<>();
            model.put("index", ipStudentRequest.getStudent().getIndex());
            emailRepository.sendMailAsync(to, "TestMailToFinanceViceDean", "email-to-aavicedean-template", null, model, null);
        }
    }

    @Override
    public void sendMailToStudentAdministrationAfterAAViceDeanApproval(InstallmentPaymentStudentRequest request) {
        String[] to = userRepository.findByRole(UserRole.FINANCE_ADMINISTRATION).stream().map(User::getEmail).toArray(String[]::new);
        Map<String, Object> model = new HashMap<>();
        model.put("index", request.getStudent().getIndex());
        model.put("type", request.getRequestSession().getRequestType());
        emailRepository.sendMailAsync(to, "TestMailToStudentAdministration", "email-to-studentadmin-template", null, model, null);
    }

    @Override
    public void sendMailToStudentOnStatusChange(InstallmentPaymentStudentRequest request) {
        String[] to = {request.getStudent().getEmail()};
        Map<String, Object> model = new HashMap<>();
        model.put("id", request.getId());
        model.put("index", request.getStudent().getIndex());
        model.put("type", request.getRequestSession().getRequestType());
        String status;
        if (request.getIsProcessed()) {
            status = "ПРОЦЕСИРАНА";
        } else if (request.getIsApproved() != null) {
            status = request.getIsApproved() ? "OДОБРЕНА" : "ОДБИЕНА";
        } else {
            status = "ОДОБРЕНА ОД ПРОФЕСОР";
        }
        model.put("status", status);
        emailRepository.sendMailAsync(to, "TestMailToStudent", "email-to-student-template", null, model, null);
    }

    private Page<InstallmentPaymentStudentRequest> findPage(Long sessionId,
                                                            Integer pageNum,
                                                            Integer results,
                                                            Boolean isApproved,
                                                            Boolean isProcessed,
                                                            Integer installmentsNum,
                                                            String student) {


        Specification<InstallmentPaymentStudentRequest> spec = Specification
                .where(filterEquals(InstallmentPaymentStudentRequest.class, "requestSession.id", sessionId))
                .and(filterEquals(InstallmentPaymentStudentRequest.class, "isApproved", isApproved))
                .and(filterEquals(InstallmentPaymentStudentRequest.class, "isProcessed", isProcessed))
                .and(filterEquals(InstallmentPaymentStudentRequest.class, "installmentsNum", installmentsNum))
                .and(filterEquals(InstallmentPaymentStudentRequest.class, "student.index", student));

        return requestRepository.findAll(spec, getPageRequest(pageNum, results));
    }

    private PageRequest getPageRequest(int pageNum, int pageSize) {
        return PageRequest.of(pageNum - 1, pageSize, Sort.Direction.ASC, "id");
    }
}
