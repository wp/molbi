package mk.ukim.finki.molbi.service.impl;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.molbi.model.base.JoinedSubject;
import mk.ukim.finki.molbi.model.exceptions.SubjectNotFoundException;
import mk.ukim.finki.molbi.repository.JoinedSubjectRepository;
import mk.ukim.finki.molbi.service.inter.SemesterService;
import mk.ukim.finki.molbi.service.inter.SubjectService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SubjectServiceImpl implements SubjectService {
    private final JoinedSubjectRepository subjectRepository;
    private final SemesterService semesterService;

    @Override
    public JoinedSubject findById(String id) {
        return subjectRepository.findById(id).orElseThrow(() -> new SubjectNotFoundException(id));
    }


    @Override
    public List<JoinedSubject> listBySemesterType() {
        return subjectRepository.findBySemesterTypeOrderByName(semesterService.getActiveSemesterType());
    }
}