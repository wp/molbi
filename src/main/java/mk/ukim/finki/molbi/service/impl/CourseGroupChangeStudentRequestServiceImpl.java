package mk.ukim.finki.molbi.service.impl;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import mk.ukim.finki.molbi.model.base.JoinedSubject;
import mk.ukim.finki.molbi.model.base.Professor;
import mk.ukim.finki.molbi.model.base.Student;
import mk.ukim.finki.molbi.model.base.User;
import mk.ukim.finki.molbi.model.dtos.StudentRequestDto;
import mk.ukim.finki.molbi.model.enums.RequestType;
import mk.ukim.finki.molbi.model.enums.UserRole;
import mk.ukim.finki.molbi.model.exceptions.*;
import mk.ukim.finki.molbi.model.requests.CourseGroupChangeStudentRequest;
import mk.ukim.finki.molbi.model.requests.RequestSession;
import mk.ukim.finki.molbi.repository.*;
import mk.ukim.finki.molbi.service.inter.CourseGroupChangeStudentRequestService;
import mk.ukim.finki.molbi.service.inter.RequestSessionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static mk.ukim.finki.molbi.service.specification.FieldFilterSpecification.filterEquals;

@Service
@RequiredArgsConstructor
public class CourseGroupChangeStudentRequestServiceImpl implements CourseGroupChangeStudentRequestService {

    private final CourseGroupChangeStudentRequestRepository requestRepository;
    private final JoinedSubjectRepository subjectRepository;
    private final ProfessorRepository professorRepository;
    private final StudentRepository studentRepository;
    private final RequestSessionService requestSessionService;
    private final UserRepository userRepository;
    private final EmailRepository emailRepository;


    @Override
    @Transactional
    public Page<CourseGroupChangeStudentRequest> filterAndPaginateRequests(Long sessionId,
                                                                           Integer pageNum,
                                                                           Integer results,
                                                                           String subject,
                                                                           String currentProfessor,
                                                                           String newprofessor,
                                                                           Boolean isApproved,
                                                                           Boolean isProcessed,
                                                                           String student) {
        return findPage(sessionId, pageNum, results, subject, currentProfessor, newprofessor, isApproved, isProcessed, student);

    }

    @Override
    public List<CourseGroupChangeStudentRequest> getAllRequestByStudent(String index) {
        return requestRepository.findByStudentIndex(index);
    }

    @Override
    public CourseGroupChangeStudentRequest findById(Long id) {
        return requestRepository.findById(id).orElseThrow(
                () -> new CourseGroupChangeStudentRequestNotFoundException(id));
    }

    @Override
    public void create(StudentRequestDto dto) {
        if (dto.getSubject() == null || dto.getCurrentProfessor().isEmpty() || dto.getNewProfessor().isEmpty()
            || dto.getDescription().isEmpty()) {
            throw new AllFieldsNotFilledException();
        }

        RequestSession session = requestSessionService.getActiveSessionByType(RequestType.COURSE_GROUP_CHANGE);

        Student student = studentRepository.findById(dto.getStudent())
                .orElseThrow(() -> new StudentNotFoundException(dto.getStudent()));

        JoinedSubject subject = subjectRepository.findById(dto.getSubject())
                .orElseThrow(() -> new SubjectNotFoundException(dto.getSubject()));
        Professor currentProfessor = professorRepository.findById(dto.getCurrentProfessor()).orElseThrow(() -> new ProfessorNotFoundException(dto.getCurrentProfessor()));

        Professor newProfessor = professorRepository.findById(dto.getNewProfessor()).orElseThrow(() -> new ProfessorNotFoundException(dto.getNewProfessor()));

        String description = dto.getDescription();

        CourseGroupChangeStudentRequest request = new CourseGroupChangeStudentRequest();
        request.setStudent(student);
        request.setJoinedSubject(subject);
        request.setCurrentProfessor(currentProfessor);
        request.setNewProfessor(newProfessor);
        request.setDescription(description);
        request.setRequestSession(session);
        request.setIsProcessed(false);
        request.setDateCreated(LocalDate.now());
        requestRepository.save(request);
    }

    @Override
    public void edit(Long id, StudentRequestDto dto) {
        CourseGroupChangeStudentRequest request = findById(id);

        if (request.getIsApproved() != null) {
            throw new IllegalStateException("Cannot edit a reviewed request");
        }

        if (dto.getSubject() == null || dto.getCurrentProfessor().isEmpty() || dto.getNewProfessor().isEmpty()
            || dto.getDescription().isEmpty()) {
            throw new AllFieldsNotFilledException();
        }

        JoinedSubject subject = subjectRepository.findById(dto.getSubject())
                .orElseThrow(() -> new SubjectNotFoundException(dto.getSubject()));
        Professor currentProfessor = professorRepository.findById(dto.getCurrentProfessor())
                .orElseThrow(() -> new ProfessorNotFoundException(dto.getCurrentProfessor()));
        Professor newProfessor = professorRepository.findById(dto.getNewProfessor())
                .orElseThrow(() -> new ProfessorNotFoundException(dto.getNewProfessor()));
        String description = dto.getDescription();

        request.setJoinedSubject(subject);
        request.setCurrentProfessor(currentProfessor);
        request.setNewProfessor(newProfessor);
        request.setDescription(description);
        request.setDateCreated(LocalDate.now());
        request.setIsProcessed(false);
        requestRepository.save(request);
    }

    @Override
    public void delete(Long id) {
        CourseGroupChangeStudentRequest request = findById(id);

        if (request.getIsApproved() != null) {
            throw new IllegalStateException("Cannot delete a reviewed request");
        }
        requestRepository.deleteById(id);
    }

    @Override
    public void updateStatus(Long id, Boolean action, String rejectReason) {
        CourseGroupChangeStudentRequest request = findById(id);

        if (action && !request.canBeApproved()) {
            throw new RequestAlreadyProcessedOrAlreadyApprovedException(request);
        }
        if (!action && !request.canBeRejected()) {
            throw new RequestAlreadyProcessedOrAlreadyRejectedException(request);
        }

        request.setIsApproved(action);
        request.setRejectionReason(rejectReason);
        requestRepository.save(request);
        if (request.getIsApproved() != null && request.getIsApproved()) {
            sendMailToStudentAdministrationAfterAAViceDeanApproval(request);
        }
        sendMailToStudentOnStatusChange(request);
    }

    @Override
    public void markAsProcessed(Long id) {
        CourseGroupChangeStudentRequest request = findById(id);
        if (request.getIsApproved() == null || !request.canBeMarkedAsProcessed()) {
            throw new RequestNotApprovedOrAlreadyProcessedException(request);
        }

        request.setIsProcessed(true);
        request.setDateProcessed(LocalDate.now());
        requestRepository.save(request);
        sendMailToStudentOnStatusChange(request);
    }

    @Override
    public Long totalRequestBySession(Long sessionId) {
        return requestRepository.countByRequestSessionId(sessionId);
    }

    @Override
    public List<CourseGroupChangeStudentRequest> listAllForSession(RequestSession session) {
        return requestRepository.findByRequestSession(session);
    }

    @Override
    public void sendMailsToAAViceDean() {
        List<RequestSession> endedSessions = requestSessionService.getJustEndedSessions();
        List<CourseGroupChangeStudentRequest> cgcStudentRequests = new ArrayList<>();
        for (RequestSession endedSession : endedSessions) {
            cgcStudentRequests.addAll(listAllForSession(endedSession));
        }

        User aaViceDean = userRepository.findByRole(UserRole.ACADEMIC_AFFAIR_VICE_DEAN).stream().findFirst().orElseThrow();
        String[] to = {aaViceDean.getEmail()};

        for (CourseGroupChangeStudentRequest cgcStudentRequest : cgcStudentRequests) {
            Map<String, Object> model = new HashMap<>();
            model.put("index", cgcStudentRequest.getStudent().getIndex());
            emailRepository.sendMailAsync(to, "TestMailToAAViceDean", "email-to-aavicedean-template", null, model, null);
        }
    }

    @Override
    public void sendMailToStudentAdministrationAfterAAViceDeanApproval(CourseGroupChangeStudentRequest request) {
        String[] to = userRepository.findByRole(UserRole.STUDENT_ADMINISTRATION).stream().map(User::getEmail).toArray(String[]::new);
        Map<String, Object> model = new HashMap<>();
        model.put("index", request.getStudent().getIndex());
        model.put("type", request.getRequestSession().getRequestType());
        emailRepository.sendMailAsync(to, "TestMailToStudentAdministration", "email-to-studentadmin-template", null, model, null);
    }

    @Override
    public void sendMailToStudentOnStatusChange(CourseGroupChangeStudentRequest request) {
        String[] to = {request.getStudent().getEmail()};
        Map<String, Object> model = new HashMap<>();
        model.put("id", request.getId());
        model.put("index", request.getStudent().getIndex());
        model.put("type", request.getRequestSession().getRequestType());
        String status;
        if (request.getIsProcessed()) {
            status = "ПРОЦЕСИРАНА";
        } else if (request.getIsApproved() != null) {
            status = request.getIsApproved() ? "OДОБРЕНА" : "ОДБИЕНА";
        } else {
            status = "ОДОБРЕНА ОД ПРОФЕСОР";
        }
        model.put("status", status);
        emailRepository.sendMailAsync(to, "TestMailToStudent", "email-to-student-template", null, model, null);
    }

    private Page<CourseGroupChangeStudentRequest> findPage(Long sessionId,
                                                           Integer pageNum,
                                                           Integer results,
                                                           String subject,
                                                           String currentProfessor,
                                                           String newProfessor,
                                                           Boolean isApproved,
                                                           Boolean isProcessed,
                                                           String student) {


        Specification<CourseGroupChangeStudentRequest> spec = Specification
                .where(filterEquals(CourseGroupChangeStudentRequest.class, "requestSession.id", sessionId))
                .and(filterEquals(CourseGroupChangeStudentRequest.class, "subject.id", subject))
                .and(filterEquals(CourseGroupChangeStudentRequest.class, "currentProfessor.id", currentProfessor))
                .and(filterEquals(CourseGroupChangeStudentRequest.class, "newProfessor.id", newProfessor))
                .and(filterEquals(CourseGroupChangeStudentRequest.class, "isApproved", isApproved))
                .and(filterEquals(CourseGroupChangeStudentRequest.class, "isProcessed", isProcessed))
                .and(filterEquals(CourseGroupChangeStudentRequest.class, "student.index", student));

        return requestRepository.findAll(spec, getPageRequest(pageNum, results));
    }

    private PageRequest getPageRequest(int pageNum, int pageSize) {
        return PageRequest.of(pageNum - 1, pageSize, Sort.Direction.ASC, "id");
    }

}