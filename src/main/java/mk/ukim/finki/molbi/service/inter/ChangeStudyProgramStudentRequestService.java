package mk.ukim.finki.molbi.service.inter;

import jakarta.transaction.Transactional;
import mk.ukim.finki.molbi.model.dtos.StudentRequestDto;
import mk.ukim.finki.molbi.model.dtos.StudyProgramRequestDto;
import mk.ukim.finki.molbi.model.exceptions.ChangeStudyProgramStudentRequestException;
import mk.ukim.finki.molbi.model.requests.ChangeStudyProgramStudentRequest;
import mk.ukim.finki.molbi.model.requests.RequestSession;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ChangeStudyProgramStudentRequestService {

    @Transactional
    Page<ChangeStudyProgramStudentRequest> filterAndPaginateRequests
            (Long sessionId,
             Integer pageNum,
             Integer results,
             String oldStudyProgram,
             String newStudyProgram,
             Boolean isApproved,
             Boolean isProcessed,
             String student);

    @Transactional
    List<ChangeStudyProgramStudentRequest> getAllRequestByStudent(String index);

    ChangeStudyProgramStudentRequest findById(Long id);

    void create(StudentRequestDto dto) throws ChangeStudyProgramStudentRequestException;

    @Transactional
    List<StudyProgramRequestDto> importRequests(List<StudyProgramRequestDto> importRequests, Long requestSessionId);

    void edit(Long id, StudentRequestDto dto);

    void delete(Long id);

    void updateStatus(Long id, Boolean action, String rejectReason);

    void markAsProcessed(Long id);

    Long totalRequestBySession(Long sessionId);

    List<ChangeStudyProgramStudentRequest> listAllForSession(RequestSession session);

    void sendMailsToAAViceDean();

    void sendMailToStudentAdministrationAfterAAViceDeanApproval(ChangeStudyProgramStudentRequest request);

    void sendMailToStudentOnStatusChange(ChangeStudyProgramStudentRequest request);
}
