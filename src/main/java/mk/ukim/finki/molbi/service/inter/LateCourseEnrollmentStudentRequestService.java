package mk.ukim.finki.molbi.service.inter;

import jakarta.transaction.Transactional;
import mk.ukim.finki.molbi.model.dtos.StudentRequestDto;
import mk.ukim.finki.molbi.model.requests.LateCourseEnrollmentStudentRequest;
import mk.ukim.finki.molbi.model.requests.RequestSession;
import org.springframework.data.domain.Page;

import java.util.List;

public interface LateCourseEnrollmentStudentRequestService {
    @Transactional
    Page<LateCourseEnrollmentStudentRequest> filterAndPaginateRequests
            (Long sessionId, Integer pageNum, Integer results, Boolean professorApproved, Boolean isApproved,
             Boolean isProcessed, String student, String professor);

    @Transactional
    List<LateCourseEnrollmentStudentRequest> getAllRequestByStudent(String index);

    LateCourseEnrollmentStudentRequest findById(Long id);

    void create(StudentRequestDto dto);

    void edit(Long id, StudentRequestDto dto);

    void delete(Long id);

    void processApprovalByProfessor(Long id);

    void updateStatus(Long id, Boolean action, String rejectReason);

    void markAsProcessed(Long id);

    Long totalRequestBySession(Long sessionId);

    List<LateCourseEnrollmentStudentRequest> listAllForSession(RequestSession session);

    void sendMailsToProfessors();

    void sendMailToAAViceDeanAfterProfessorApproval(LateCourseEnrollmentStudentRequest request);

    void sendMailToStudentAdministrationAfterAAViceDeanApproval(LateCourseEnrollmentStudentRequest request);


    void sendMailToStudentOnStatusChange(LateCourseEnrollmentStudentRequest request);

}
