package mk.ukim.finki.molbi.service.impl;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.molbi.model.base.Semester;
import mk.ukim.finki.molbi.model.enums.SemesterState;
import mk.ukim.finki.molbi.model.enums.SemesterType;
import mk.ukim.finki.molbi.model.exceptions.NoActiveSemesterException;
import mk.ukim.finki.molbi.repository.SemesterRepository;
import mk.ukim.finki.molbi.service.inter.SemesterService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SemesterServiceImpl implements SemesterService {
    private final SemesterRepository repository;

    @Override
    public List<Semester> findAll() {
        return repository.findAll();
    }


    @Override
    public Semester findById(String code) {
        return repository.findById(code).orElseThrow();
    }

    @Override
    public SemesterType getActiveSemesterType() {
        Semester semester = repository.findByState(SemesterState.STARTED);
        if (semester == null) {
            throw new NoActiveSemesterException();
        }
        return repository.findByState(SemesterState.STARTED).getSemesterType();
    }
}
