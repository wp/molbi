package mk.ukim.finki.molbi.service.impl;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.molbi.model.base.JoinedSubject;
import mk.ukim.finki.molbi.model.base.Professor;
import mk.ukim.finki.molbi.model.base.StudyProgramSubjectProfessor;
import mk.ukim.finki.molbi.model.exceptions.ProfessorNotFoundException;
import mk.ukim.finki.molbi.repository.ProfessorRepository;
import mk.ukim.finki.molbi.repository.StudyProgramSubjectProfessorRepository;
import mk.ukim.finki.molbi.service.inter.ProfessorService;
import mk.ukim.finki.molbi.service.inter.SubjectService;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProfessorServiceImpl implements ProfessorService {
    private final ProfessorRepository professorRepository;
    private final StudyProgramSubjectProfessorRepository subjectProfessorRepository;
    private final SubjectService subjectService;

    @Override
    public List<Professor> listAll() {
        return professorRepository.findAllByOrderByNameAsc();
    }

    @Override
    public Professor findById(String id) {
        return professorRepository.findById(id).orElseThrow(() -> new ProfessorNotFoundException(id));
    }

    @Override
    public List<Professor> findBySubjectId(String selectedSubject) {
        JoinedSubject subject = subjectService.findById(selectedSubject);
        String subjectId = subject.getMainSubject().getId();
        return subjectProfessorRepository.findByStudyProgramSubjectSubjectId(subjectId)
                .stream()
                .map(StudyProgramSubjectProfessor::getProfessor)
                .distinct()
                .sorted(Comparator.comparing(Professor::getName))
                .toList();
    }
}
