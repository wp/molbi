package mk.ukim.finki.molbi.service.impl;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.molbi.model.base.Student;
import mk.ukim.finki.molbi.model.base.StudyProgram;
import mk.ukim.finki.molbi.model.base.StudyProgramDetails;
import mk.ukim.finki.molbi.model.enums.StudyCycle;
import mk.ukim.finki.molbi.model.exceptions.StudyProgramNotFoundException;
import mk.ukim.finki.molbi.repository.StudyProgramDetailsRepository;
import mk.ukim.finki.molbi.repository.StudyProgramRepository;
import mk.ukim.finki.molbi.service.inter.StudentService;
import mk.ukim.finki.molbi.service.inter.StudyProgramService;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class StudyProgramServiceImpl implements StudyProgramService {
    private final StudyProgramRepository studyProgramRepository;
    private final StudentService studentService;
    private final StudyProgramDetailsRepository spDetailsRepository;

    @Override
    public StudyProgram findById(String code) {
        return studyProgramRepository.findById(code).orElseThrow(() -> new StudyProgramNotFoundException(code));
    }

    @Override
    public List<StudyProgram> listAll() {
        return studyProgramRepository.findAll();
    }

    @Override
    public List<StudyProgramDetails> getStudentAvailableStudyPrograms(String index) {
        Student student = studentService.findByIndex(index);
        String code = student.getStudyProgram().getCode();
        StudyProgramDetails spd = spDetailsRepository.findById(code)
                .orElseThrow(() -> new StudyProgramNotFoundException(code));
        List<StudyProgramDetails> studyProgramDetails = spDetailsRepository
                .findByStudyCycleAndAccreditationYearGreaterThanEqualOrderByAccreditationYearDesc(
                        StudyCycle.UNDERGRADUATE, spd.getAccreditation().getYear());
        return studyProgramDetails.stream().filter(sp -> !sp.getId().equals(code)).toList();
    }

    @Override
    public Map<String, String> getMappedStudentPrograms(String index) {
        Map<String, String> map = new LinkedHashMap<>();
        List<StudyProgramDetails> spDetails = getStudentAvailableStudyPrograms(index);
        List<StudyProgram> studyPrograms = spDetails.stream().map(sp -> findById(sp.getStudyProgram().getCode())).toList();
        for (int i = 0; i < studyPrograms.size(); i++) {
            String value = studyPrograms.get(i).getName() + " (" + spDetails.get(i).getAccreditation().getYear() + ")";
            if (map.containsValue(value)) {
                continue;
            }
            map.put(studyPrograms.get(i).getCode(), studyPrograms.get(i).getName() + " (" + spDetails.get(i).getAccreditation().getYear() + ")");
        }

        return map;
    }
}
