package mk.ukim.finki.molbi.service.inter;

import mk.ukim.finki.molbi.model.base.JoinedSubject;

import java.util.List;

public interface SubjectService {
    JoinedSubject findById(String id);

    List<JoinedSubject> listBySemesterType();
}
