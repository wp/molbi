package mk.ukim.finki.molbi.service.inter;

import mk.ukim.finki.molbi.model.base.Semester;
import mk.ukim.finki.molbi.model.enums.SemesterType;

import java.util.List;

public interface SemesterService {
    List<Semester> findAll();

    Semester findById(String code);

    SemesterType getActiveSemesterType();

}
