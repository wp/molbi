package mk.ukim.finki.molbi.service.inter;

import jakarta.transaction.Transactional;
import mk.ukim.finki.molbi.model.dtos.StudentRequestDto;
import mk.ukim.finki.molbi.model.requests.CourseEnrollmentWithoutRequirementsStudentRequest;
import mk.ukim.finki.molbi.model.requests.RequestSession;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CourseEnrollmentWithoutRequirementsStudentRequestService {

    @Transactional
    Page<CourseEnrollmentWithoutRequirementsStudentRequest> filterAndPaginateRequests
            (Long sessionId,
             int pageNum,
             int results,
             String subject,
             Boolean isApproved,
             Boolean isProcessed,
             String student);

    @Transactional
    List<CourseEnrollmentWithoutRequirementsStudentRequest> getAllRequestByStudent(String index);

    CourseEnrollmentWithoutRequirementsStudentRequest findById(Long id);

    void create(StudentRequestDto dto);

    void edit(Long id, StudentRequestDto dto);

    void delete(Long id);

    void updateStatus(Long id, Boolean action, String rejectReason);

    void markAsProcessed(Long id);

    Long totalRequestBySession(Long sessionId);

    List<CourseEnrollmentWithoutRequirementsStudentRequest> listAllForSession(RequestSession session);

    void sendMailsToAAViceDean();

    void sendMailToStudentAdministrationAfterAAViceDeanApproval(CourseEnrollmentWithoutRequirementsStudentRequest request);

    void sendMailToStudentOnStatusChange(CourseEnrollmentWithoutRequirementsStudentRequest request);
}
