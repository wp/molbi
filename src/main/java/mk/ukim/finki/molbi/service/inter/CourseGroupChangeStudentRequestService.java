package mk.ukim.finki.molbi.service.inter;

import jakarta.transaction.Transactional;
import mk.ukim.finki.molbi.model.dtos.StudentRequestDto;
import mk.ukim.finki.molbi.model.requests.CourseGroupChangeStudentRequest;
import mk.ukim.finki.molbi.model.requests.RequestSession;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CourseGroupChangeStudentRequestService {
    @Transactional
    Page<CourseGroupChangeStudentRequest> filterAndPaginateRequests
            (Long sessionId,
             Integer pageNum,
             Integer results,
             String subject,
             String oldProfessor,
             String newProfessor,
             Boolean isApproved,
             Boolean isProcessed,
             String student);

    @Transactional
    List<CourseGroupChangeStudentRequest> getAllRequestByStudent(String index);

    CourseGroupChangeStudentRequest findById(Long id);

    void create(StudentRequestDto dto);

    void edit(Long id, StudentRequestDto dto);

    void delete(Long id);

    void updateStatus(Long id, Boolean action, String rejectReason);

    void markAsProcessed(Long id);

    Long totalRequestBySession(Long sessionId);

    List<CourseGroupChangeStudentRequest> listAllForSession(RequestSession session);

    void sendMailsToAAViceDean();

    void sendMailToStudentAdministrationAfterAAViceDeanApproval(CourseGroupChangeStudentRequest request);

    void sendMailToStudentOnStatusChange(CourseGroupChangeStudentRequest request);

}
