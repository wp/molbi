package mk.ukim.finki.molbi.service.scheduled;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.molbi.service.inter.*;
import org.springframework.scheduling.annotation.Scheduled;

@RequiredArgsConstructor
public class ScheduledEvent {

    private final LateCourseEnrollmentStudentRequestService lceService;
    private final GeneralStudentRequestService genService;
    private final CourseEnrollmentWithoutRequirementsStudentRequestService cwrService;
    private final CourseGroupChangeStudentRequestService cgcService;
    private final ChangeStudyProgramStudentRequestService cspService;
    private final InstallmentPaymentStudentRequestService ipService;


    @Scheduled(cron = "0 0 0 * * *")
    public void scheduleEvents() {
        lceService.sendMailsToProfessors();
        genService.sendMailsToAAViceDean();
        cgcService.sendMailsToAAViceDean();
        cspService.sendMailsToAAViceDean();
        cwrService.sendMailsToAAViceDean();
        ipService.sendMailsToFinanceViceDean();
    }

}
