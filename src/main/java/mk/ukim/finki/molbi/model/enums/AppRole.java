package mk.ukim.finki.molbi.model.enums;

public enum AppRole {
    STUDENT, PROFESSOR, ADMIN, FINANCES_VICE_DEAN, GUEST;

    public String roleName() {
        return "ROLE_" + this.name();
    }
}
