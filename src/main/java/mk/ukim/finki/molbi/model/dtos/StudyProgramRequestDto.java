package mk.ukim.finki.molbi.model.dtos;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonPropertyOrder({"id", "student", "description", "requestSession", "isApproved", "dateCreated", "dateProcessed",
        "rejectionReason", "isProcessed", "newStudyProgram", "oldStudyProgram"})
public class StudyProgramRequestDto {
    private Long id;

    private String student;

    private String description;

    private Long requestSession;

    private Boolean isApproved;

    private String dateCreated;

    private String dateProcessed;

    private String rejectionReason;

    private Boolean isProcessed;

    private String newStudyProgram;

    private String oldStudyProgram;
}
