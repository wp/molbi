package mk.ukim.finki.molbi.model.exceptions;

public class NoActiveSemesterException extends RuntimeException {
    public NoActiveSemesterException() {
        super("No active semester.");
    }
}
