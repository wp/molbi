package mk.ukim.finki.molbi.web;


import lombok.AllArgsConstructor;
import mk.ukim.finki.molbi.config.FacultyUserDetails;
import mk.ukim.finki.molbi.model.requests.LateCourseEnrollmentStudentRequest;
import mk.ukim.finki.molbi.service.inter.LateCourseEnrollmentStudentRequestService;
import org.springframework.data.domain.Page;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@AllArgsConstructor
@RequestMapping("/professor")
public class ProfessorController {

    private final LateCourseEnrollmentStudentRequestService lceService;

    @GetMapping
    public String getRequests(Model model,
                              @RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "20") Integer results,
                              @RequestParam(required = false) Boolean professorApproved,
                              @RequestParam(required = false) Boolean isApproved,
                              @RequestParam(required = false) Boolean isProcessed,
                              @RequestParam(required = false) String student,
                              @AuthenticationPrincipal FacultyUserDetails principal) {

        Page<LateCourseEnrollmentStudentRequest> page;
        String professorId = principal.getUsername();
        page = lceService.filterAndPaginateRequests(null, pageNum, results, professorApproved, isApproved,
                isProcessed, student, professorId);

        model.addAttribute("page", page);
        model.addAttribute("professorId", professorId);

        return "teacher-requests/listTeacherRequests";
    }

    @PostMapping("/professorApprove/{id}")
    public String approveByProfessorRequest(@PathVariable Long id) {
        lceService.processApprovalByProfessor(id);
        return "redirect:/professor";
    }
}
