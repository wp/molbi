# Molbi

## Description

Molbi is a comprehensive student request management system designed to streamline the submission, management, and
processing of various student requests at FINKI. This project aims to optimize and automate the student request
management workflow, ensuring efficient use of resources and providing a user-friendly interface for managing student
request.
